package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {
    //stores and gets the driver (eg ChromeDriver)
    WebDriver driver = Utils.getDriver();

    LoginPage loginPage = new LoginPage(driver);
    ToolsPage toolsPage = new ToolsPage(driver);

    @Before
    public void setUp() {
        //this had the driver in before, but I have decided to put it above the @before
    }

    @After
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

    @Given("I navigate to the login page")
    public void i_navigate_to_the_login_page() {
        // Write code here that turns the phrase above into concrete actions
        // Step 1 ACTION: Open our web application in the browser
        driver.get(Utils.getURL());
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page", "We're not on the Login Page or its title has changed");
    }

    @When("I enter the login details for a {string}")
    public void i_enter_the_login_details_for_a(String userType) {
        // Write code here that turns the phrase above into concrete actions
        switch (userType) {
            case "invalidUser":
                loginPage.enterUserName("attacker");
                loginPage.enterPassword("donotletmein");
                loginPage.clickLoginButton();
                break;
            case "validUser":
                loginPage.enterUserName("tester");
                loginPage.enterPassword("letmein");
                loginPage.clickLoginButton();
                break;
            default:
                Assert.fail("The test data is wrong - the only values that can be accepted are 'validUser' or 'invalidUser'. You entered: " + userType);
                break;
        }
    }

    @Then("I can see the following message: {string}")
    public void i_can_see_the_following_message(String validationMessage) {
        // Write code here that turns the phrase above into concrete actions
        switch (validationMessage) {
            case "Username or Password is incorrect":
                Assert.assertTrue(loginPage.checkForFailedLoginWarning().contains(validationMessage));
                break;
            case "Login Successful":
                Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains(validationMessage));
                break;
            default:
                Assert.fail("The test data is wrong. You entered: " + validationMessage);
                break;
        }
    }
}
