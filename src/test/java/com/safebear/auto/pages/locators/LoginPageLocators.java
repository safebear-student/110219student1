package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

/**
 * The @Data tag creates a 'get' and a 'set' command for each of my By variables
 * the 'get' command allows me to 'get' the value of these variables from another class
 * the 'set' command allows me to 'set' the value of these variables from another class
 */
@Data
public class LoginPageLocators {

// "contains" example: http://toolslist.safebear.co.uk:8080/

    //fields
    private By usernameLocator = By.id("username");
    private By passwordLocator = By.id("password");

    //buttons
    private By loginButtonLocator = By.id("enter");

    //messages
    //the '.' in the below xpath is searching for text
    private By failedLoginMessage = By.xpath(".//b[.='WARNING: Username or Password is incorrect']");

}
