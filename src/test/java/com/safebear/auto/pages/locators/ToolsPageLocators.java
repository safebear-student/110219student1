package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

/**
 * The @Data tag creates a 'get' and a 'set' command for each of my By variables
 * the 'get' command allows me to 'get' the value of these variables from another class
 * the 'set' command allows me to 'set' the value of these variables from another class
 */
@Data
public class ToolsPageLocators {

    //messages
    //the '.' in the below xpath is searching for text
    private By successfulLoginMessage = By.xpath(".//b[contains(.,'Successful')]");
    private By additionalExercises = By.xpath(".//a[.='Additional Exercises']");


}
