package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import com.safebear.auto.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

//Will ensure that the LoginPage will ask for a 'driver'
@RequiredArgsConstructor
public class ToolsPage {

    //Link our LoginPageLocators to this file
    ToolsPageLocators locators = new ToolsPageLocators();

    @NonNull
    WebDriver driver;


    //CHECK Commands

    //Checks the 'Login Successful' message
    public String checkForLoginSuccessfulMessage() {
        return Utils.waitForElement(locators.getSuccessfulLoginMessage(), driver).getText();
        /**
         * I have commented the below out as stated on page 100 in play manual:
         * return driver.findElement(locators.getSuccessfulLoginMessage()).getText();
         */
    }
/**
    //Clicks on the Additional Exercises button
    public void clickAdditionalButton() {
        Utils.waitForElement(locators.getAdditionalExercises(), driver).click();
    }
*/
 }