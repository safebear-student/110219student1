package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import com.safebear.auto.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

import java.util.SplittableRandom;

//Will ensure that the LoginPage will ask for a 'driver'
@RequiredArgsConstructor
public class LoginPage {

    //Link our LoginPageLocators to this file
    LoginPageLocators locators = new LoginPageLocators();

    //Ensures that we have a 'driver' (e.g. ChromeDriver or FirefoxDriver)
    @NonNull
    WebDriver driver;

    //ACTION Commands

    //Returns the page title
    public String getPageTitle(){
        return driver.getTitle();
    }

    //Enters the username into the username field
    public void enterUserName(String username){
        Utils.waitForElement(locators.getUsernameLocator(),driver).sendKeys(username);
        /**
         * I have commented the below out as stated on page 100 in play manual:
         * driver.findElement(locators.getUsernameLocator()).sendKeys(username);
         */
    }

    //Enters the password into the password field
    public void enterPassword(String password){
        Utils.waitForElement(locators.getPasswordLocator(),driver).sendKeys(password);
        /**
         * I have commented the below out as stated on page 100 in play manual:
         * driver.findElement(locators.getUsernameLocator()).sendKeys(password);
         */
    }

    //Clicks on the Login button
    public void clickLoginButton(){
        Utils.waitForElement(locators.getLoginButtonLocator(),driver).click();
        /**
         * I have commented the below out as stated on page 100 in play manual:
         * driver.findElement(locators.getLoginButtonLocator()).click();
         */
    }

    //CHECK Commands

    //Checks the 'Login Failed' message
    public String checkForFailedLoginWarning(){
        return Utils.waitForElement(locators.getFailedLoginMessage(),driver).getText();
        /**
         * I have commented the below out as stated on page 100 in play manual:
        return driver.findElement(locators.getFailedLoginMessage()).getText();
         */
    }
}
